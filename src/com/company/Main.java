package com.company;

import model.Board;
import model.Game;

public class Main {

    public static void main(String[] args) {
	    if (args.length != 1 || Integer.parseInt(args[0]) <3) {
            throw new IllegalArgumentException("Usage: Pass the board size larger than 2");
        }

        Game game = new Game(Integer.parseInt(args[0]));
        game.performGame();
    }
}
