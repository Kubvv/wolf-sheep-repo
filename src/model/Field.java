package model;

public class Field {

    private char occupier;

    public Field(char occupier) {
        this.occupier = occupier;
    }

    public char getOccupier() {
        return occupier;
    }

    public void setOccupier(char occupier) {
        this.occupier = occupier;
    }
}
