package model;

import logic.BoardLogic;

import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Game {

    private final Board board;
    private boolean isEnd; //Indicates if the game has ended or not
    private final BoardLogic boardLogic;
    private int roundCounter; //Counts the number of performed moves (wolf - even, sheep - odd)
    private final Pattern pattern; // pattern is used in checking the regular expression of user input

    public Game(int x) {
        board = new Board(x);
        isEnd = false;
        this.boardLogic = new BoardLogic();
        roundCounter = 0;
        pattern = Pattern.compile("^[0-9]+-[0-9]+[ ]*->[ ]*[0-9]+-[0-9]+$");
    }

    /* Runs the game until one of the player wins. After a win
     * prints an appropriate message */
    public void performGame() {
        Coordinates tmp;
        Coordinates from = new Coordinates();
        Coordinates to = new Coordinates();
        board.printBoard();
        board.setWolfPosition();

        while (!isEnd) {
            System.out.println((roundCounter % 2 == 0 ? "Wolf's" : "Sheep's") + " turn");
            System.out.println("y_from-x_from -> y_to-x_to");
            tmp = getUserInput(from, to);

            if (roundCounter % 2 == 0) {
                board.setWolfPosition(tmp);
            } else {
                sheepPositionUpdate(from, to);
            }

            board.printBoard();
            isEnd = boardLogic.checkWin(board, roundCounter, board.getWolfPosition(), board.getSheepPositions());
            roundCounter++;
        }
        System.out.println((roundCounter % 2 == 1 ? "wolf" : "sheep") + " player won");
    }

    /* Gets user input and keeps running until correct command is detected */
    private Coordinates getUserInput(Coordinates from, Coordinates to) {
        boolean flag;
        Coordinates result;

        do {
            Scanner scanner = new Scanner(System.in);
            String moveParam = scanner.nextLine();
            Matcher matcher = pattern.matcher(moveParam);
            if (matcher.find()) {
                moveParam = moveParam.replace(" ", "");
                setCoordinates(from, to, moveParam.split("->"));
            }
            flag = (result = boardLogic.Move(board, roundCounter, from, to)) == null;
            if(flag)
                System.out.println("Wrong coordinates");
        } while (flag);

        return result;
    }

    /* Sets the coordinates from parsed user input */
    private void setCoordinates(Coordinates from, Coordinates to, String[] arguments) {
        from.setX(Integer.parseInt(arguments[0].split("-")[0]));
        from.setY(Integer.parseInt(arguments[0].split("-")[1]));
        to.setX(Integer.parseInt(arguments[1].split("-")[0]));
        to.setY(Integer.parseInt(arguments[1].split("-")[1]));
    }

    private void sheepPositionUpdate(Coordinates from, Coordinates to) {
        for(Coordinates sheepPos : board.getSheepPositions()) {
            if (sheepPos.equals(from)) {
                sheepPos.setX(to.getX());
                sheepPos.setY(to.getY());
                break;
            }
        }
    }
}