package model;

import jdk.swing.interop.SwingInterOpUtils;

import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Board {
    private final Field[][] fields; //holds current board state
    private final int size; //describes the length of board's row and column
    private Coordinates wolfPosition;
    private List<Coordinates> sheepPositions;

    public Board(int x) {
        sheepPositions = new LinkedList<>();
        size = x;
        fields = new Field[x][x];
        generateBoard();
    }

    public Coordinates getWolfPosition() {
        return wolfPosition;
    }

    public void setWolfPosition(Coordinates wolfPosition) {
        this.wolfPosition.setX(wolfPosition.getX());
        this.wolfPosition.setY(wolfPosition.getY());
    }

    public List<Coordinates> getSheepPositions() {
        return sheepPositions;
    }

    public int getSize(){
        return this.size;
    }

    public Field[][] getFields() {
        return fields;
    }

    /* Prints interspace for clearer view of the game board */
    private void printInterspace() {
        System.out.println();
        System.out.println("=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=");
        System.out.println();
    }

    /* Prints the current board state as well as columns and rows numbers */
    public void printBoard() {
        printInterspace();

        for (int i = 0; i < size; i++) {
            System.out.print(i + " ");
            for (int j = 0; j < size; j++) {
                System.out.print(fields[i][j].getOccupier() + " ");
            }
            System.out.println();
        }
        System.out.print("  ");
        for (int k = 0; k < size; k++) {
            System.out.print(k + " ");
        }
        System.out.println();

        printInterspace();
    }

    /* Sets the starting position of the wolf player by placing it in appropriate column
     * specified by user. This function asks user for an even numbered column and repeats
     * the request if user passes an illegal value. After passing a correct number
     * setWolfPosition reprints the board */
    public void setWolfPosition() {
        Scanner sc = new Scanner(System.in);
        int pos = 0;
        boolean eoi = false;

        while (!eoi) {
            System.out.println("Choose wolf position using an even column number: ");
            try {
                pos = sc.nextInt();
                if (pos < 0 || pos >= size || pos % 2 == 1) {
                    System.out.println("Specify an even number in board size range");
                }
                else {
                    eoi = true;
                }
            } catch (InputMismatchException e) {
                sc.nextLine();
                System.out.println("Specify a number");
            }
        }

        fields[size - 1][pos].setOccupier('w');
        printBoard();
        wolfPosition = new Coordinates(pos, size-1);
    }

    private void generateBoard() {
        /* Used in determining sheep initial positions
         * so that it matches wolf's tile color */
        int target = size % 2 == 1 ? 0 : 1;

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (i == 0 && target == j % 2) {
                    fields[i][j] = new Field('s');
                    sheepPositions.add(new Coordinates(i, j));
                }
                else {
                    fields[i][j] = new Field('-');
                }
            }
        }
    }
}
