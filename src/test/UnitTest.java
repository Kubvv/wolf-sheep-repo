package test;

import logic.BoardLogic;
import model.Board;
import model.Coordinates;
import model.Game;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class UnitTest {

    private Board board = new Board(4);
    private BoardLogic boardLogic = new BoardLogic();


    @Test
    public void setWolfTest() {
        InputStream sysInBackup = System.in; // backup System.in to restore it later
        ByteArrayInputStream in = new ByteArrayInputStream(("hello im a wrong set of coordinates\n" +
                                                            "me too\n" + "3\n" + "2\n").getBytes());
        System.setIn(in);

        board.setWolfPosition();
        assertEquals('w', board.getFields()[3][2].getOccupier());
        System.setIn(sysInBackup);
    }

    @Test
    public void moveTest() {
        /* Check if Move correctly identifies coordinates out of bounds */
        Coordinates from = new Coordinates(4,4);
        Coordinates to = new Coordinates(3,3);
        assertNull(boardLogic.Move(board, 1, from, to));

        /* Check if a move to an not adjacent tile is legal */
        from.setCoordinates(0,1);
        to.setCoordinates(1,3);
        assertNull(boardLogic.Move(board, 1, from, to));

        /* Check if sheep can move on to the tile if it is empty  */
        from.setCoordinates(0,1);
        to.setCoordinates(1,2);
        assertEquals(new Coordinates(1,2), boardLogic.Move(board, 1, from, to));

        /* Check if sheep can move on to the tile if it isn't empty */
        from.setCoordinates(0,3);
        to.setCoordinates(1,2);
        assertNull(boardLogic.Move(board, 1, from, to));
    }

    @Test
    public void checkIntegrityTest() {
        /* Check if sheep can move on to the tile if it is empty  */
        Coordinates from = new Coordinates(0,1);
        Coordinates to = new Coordinates(1,2);
        assertEquals(new Coordinates(1,2), boardLogic.Move(board, 1, from, to));
        from.setCoordinates(1,2);
        to.setCoordinates(2,3);
        assertEquals(new Coordinates(2,3), boardLogic.Move(board, 1, from, to));
    }

    @Test
    public void checkWinTest() {
        List<Coordinates> sheepCor = new ArrayList<Coordinates>();
        sheepCor.add(new Coordinates(0,1));
        sheepCor.add(new Coordinates(0,3));
        board.getFields()[2][1].setOccupier('w');

        /* No winner */
        assertFalse(boardLogic.checkWin(board, 0, new Coordinates(2,1), sheepCor));
        /* Wolf is in the first row, wolf should win */
        assertTrue(boardLogic.checkWin(board, 0, new Coordinates(0,1), sheepCor));

        board.getFields()[3][0].setOccupier('w');
        board.getFields()[2][1].setOccupier('s');
        board.getFields()[0][1].setOccupier('-');

        sheepCor.set(0, new Coordinates(2,1));
        /* Wolf is blocked, sheep should win */
        assertTrue(boardLogic.checkWin(board, 1, new Coordinates(3,0), sheepCor));

        board.getFields()[2][1].setOccupier('w');
        board.getFields()[3][0].setOccupier('s');
        board.getFields()[0][3].setOccupier('-');
        board.getFields()[1][0].setOccupier('s');

        sheepCor.set(0, new Coordinates(3,0));
        sheepCor.set(1, new Coordinates(1,0));

        /* Sheep are blocked, wolf should win */
        assertTrue(boardLogic.checkWin(board, 2, new Coordinates(2,1), sheepCor));
    }
}
