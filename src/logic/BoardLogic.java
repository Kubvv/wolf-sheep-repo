package logic;

import model.*;

import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BoardLogic {

    /* Reads user input and performs the move if given set of coordinates
     * is correct. Returns true if move was performed and false otherwise. */
    public Coordinates Move(Board board, int round, Coordinates from, Coordinates to) {
        if (!isMovePossible(board, from, to, round)) return null;

        board.getFields()[to.getX()][to.getY()].setOccupier(round % 2 == 0 ? 'w' : 's');
        board.getFields()[from.getX()][from.getY()].setOccupier('-');

        return to;
    }

    /* Checks if a move from a field to b field is legal, aka:
     * - it is not out of bounds
     * - correct player pawn moves to an empty field
     * - it is not a returning movement if a player is sheep
     * If all these conditions are met, it returns true, otherwise it returns false */
    private boolean isMovePossible(Board board, Coordinates from, Coordinates to, int round) {
        if (board.getSize() <= from.getX() || board.getSize() <= to.getX() || board.getSize() <= from.getY() || board.getSize() <= to.getY())
            return false;

        // Regex checks negative values, but checkWin does not
        if (to.getX() < 0 || to.getY() < 0)
            return false;

        if (board.getFields()[to.getX()][to.getY()].getOccupier() != '-')
            return false;

        if (board.getFields()[from.getX()][from.getY()].getOccupier() == 's' && round % 2 == 1) {
            return Math.abs(to.getY() - from.getY()) == 1 && to.getX() - from.getX() == 1;
        } else if (board.getFields()[from.getX()][from.getY()].getOccupier() == 'w' && round % 2 == 0) {

            return Math.abs(to.getX() - from.getX()) == 1 && Math.abs(from.getY() - to.getY()) == 1;
        }

        return false;
    }

    /* Checks if any player has won the game by analyzing the board state
     * returns true if wolf is in the first row or has no possible moves, otherwise
     * returns false */
    public boolean checkWin(Board board, int round, Coordinates wolfPosition, List<Coordinates> sheepPositions) {

        if (round % 2 == 0) {
            return wolfPosition.getX() == 0 || !checkSheepMobility(board,sheepPositions);
        } else {
            return !(isMovePossible(board, wolfPosition, new Coordinates(wolfPosition.getX() + 1, wolfPosition.getY() + 1), 0) ||
                    isMovePossible(board, wolfPosition, new Coordinates(wolfPosition.getX() + 1, wolfPosition.getY() - 1), 0) ||
                    isMovePossible(board, wolfPosition, new Coordinates(wolfPosition.getX() - 1, wolfPosition.getY() + 1), 0) ||
                    isMovePossible(board, wolfPosition, new Coordinates(wolfPosition.getX() - 1, wolfPosition.getY() - 1), 0));
        }
    }

    private boolean checkSheepMobility(Board board, List<Coordinates> sheepPositions) {
        for (Coordinates c : sheepPositions) {
            if (isMovePossible(board, c, new Coordinates(c.getX() + 1, c.getY() + 1), 1) ||
                    isMovePossible(board, c, new Coordinates(c.getX() + 1, c.getY() - 1), 1))
                return true;
        }
        return false;
    }
}
